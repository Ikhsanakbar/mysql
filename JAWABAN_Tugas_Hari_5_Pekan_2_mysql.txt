Microsoft Windows [Version 10.0.18362.1016]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\Users\ikhsa>cd C:\XAMP\mysql\bin

C:\XAMP\mysql\bin>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 78
Server version: 10.4.14-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.


[[SOAL 1]]

MariaDB [(none)]> create database warungpedia;
Query OK, 1 row affected (0.005 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
| warungpedia        |
+--------------------+
7 rows in set (0.005 sec)


[[SOAL 2]]

MariaDB [(none)]> use warungpedia;
Database changed
MariaDB [warungpedia]> CREATE TABLE users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar (255),
    -> PRIMARY KEY (id));
Query OK, 0 rows affected (0.038 sec)

MariaDB [warungpedia]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.026 sec)

MariaDB [warungpedia]> CREATE TABLE categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> PRIMARY KEY (id));
Query OK, 0 rows affected (0.067 sec)

MariaDB [warungpedia]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.027 sec)

MariaDB [warungpedia]> CREATE TABLE items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> categoryID int,
    -> PRIMARY KEY (id),
    -> FOREIGN KEY (categoryID) REFERENCES categories (id));
Query OK, 0 rows affected (0.039 sec)

MariaDB [warungpedia]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| categoryID  | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.029 sec)

[[SOAL 3]]

MariaDB [warungpedia]> INSERT INTO users (name, email, password)
    -> VALUES ("John Doe", "john@doe.com", "john123");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> INSERT INTO users (name, email, password)
    ->     -> VALUES ("Jane Doe", "jane@doe.com", "jenita123");
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '-> VALUES ("Jane Doe", "jane@doe.com", "jenita123")' at line 2
MariaDB [warungpedia]> INSERT INTO users(name, email, password)
    -> VALUES ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> SELECT*FROM users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

MariaDB [warungpedia]> INSERT INTO categories (name)
    -> VALUES ("gadget");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> INSERT INTO categories (name)
    -> VALUES ("cloth");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> INSERT INTO categories (name)
    -> VALUES ("men");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> INSERT INTO categories (name)
    -> VALUES ("women");
Query OK, 1 row affected (0.010 sec)

MariaDB [warungpedia]> INSERT INTO categories (name)
    -> VALUES ("branded");
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> SELECT*FROM categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [warungpedia]> INSERT INTO items (name, description, price, stock, categoryID)
    -> VALUES ("Sumsang b40", "hape keren dari merek Sumsang", 4000000, 100, 1);
Query OK, 1 row affected (0.018 sec)

MariaDB [warungpedia]> INSERT INTO items (name, description, price, stock, categoryID)
    -> VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
Query OK, 1 row affected (0.017 sec)

MariaDB [warungpedia]> INSERT INTO items (name, description, price, stock, categoryID)
    -> VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 1 row affected (0.016 sec)

MariaDB [warungpedia]> SELECT*FROM items;
+----+-------------+-----------------------------------+---------+-------+------------+
| id | name        | description                       | price   | stock | categoryID |
+----+-------------+-----------------------------------+---------+-------+------------+
|  1 | Sumsang b40 | hape keren dari merek Sumsang     | 4000000 |   100 |          1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |          2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |          1 |
+----+-------------+-----------------------------------+---------+-------+------------+
3 rows in set (0.001 sec)


[[SOAL 4a]]

MariaDB [warungpedia]> SELECT id, name, email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)


[[SOAL 4b.1]]

MariaDB [warungpedia]> SELECT*FROM items WHERE price >=1000000;
+----+-------------+-----------------------------------+---------+-------+------------+
| id | name        | description                       | price   | stock | categoryID |
+----+-------------+-----------------------------------+---------+-------+------------+
|  1 | Sumsang b40 | hape keren dari merek Sumsang     | 4000000 |   100 |          1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |          1 |
+----+-------------+-----------------------------------+---------+-------+------------+
2 rows in set (0.001 sec)

[[SOAL 4b.2]]

MariaDB [warungpedia]> SELECT*FROM items WHERE name LIKE "%sang%";
+----+-------------+-------------------------------+---------+-------+------------+
| id | name        | description                   | price   | stock | categoryID |
+----+-------------+-------------------------------+---------+-------+------------+
|  1 | Sumsang b40 | hape keren dari merek Sumsang | 4000000 |   100 |          1 |
+----+-------------+-------------------------------+---------+-------+------------+
1 row in set (0.001 sec)


[[SOAL 4c]]

MariaDB [warungpedia]> SELECT items.name, items.description, items.price, items.stock, items.categoryID, categories.name FROM categories INNER JOIN items ON categories.id=items.categoryID;
+-------------+-----------------------------------+---------+-------+------------+--------+
| name        | description                       | price   | stock | categoryID | name   |
+-------------+-----------------------------------+---------+-------+------------+--------+
| Sumsang b40 | hape keren dari merek Sumsang     | 4000000 |   100 |          1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |          2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |          1 | gadget |
+-------------+-----------------------------------+---------+-------+------------+--------+
3 rows in set (0.001 sec)


[[SOAL 5]]

MariaDB [warungpedia]> UPDATE items SET price=2500000 WHERE id=1;
Query OK, 1 row affected (0.016 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [warungpedia]> SELECT*FROM items;
+----+-------------+-----------------------------------+---------+-------+------------+
| id | name        | description                       | price   | stock | categoryID |
+----+-------------+-----------------------------------+---------+-------+------------+
|  1 | Sumsang b40 | hape keren dari merek Sumsang     | 2500000 |   100 |          1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |          2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |          1 |
+----+-------------+-----------------------------------+---------+-------+------------+
3 rows in set (0.001 sec)